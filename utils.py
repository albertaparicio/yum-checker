import time
from functools import wraps

from tqdm import tqdm


def log(msg, logger=None):
    if logger:
        logger.warning(msg)
    else:
        tqdm.write(msg)


def retry(exceptions, total_tries=4, initial_wait=0.5, backoff_factor=2, logger=None):
    """
    calling the decorated function applying an exponential backoff.
    Args:
        exceptions: Exeption(s) that trigger a retry, can be a tuple
        total_tries: Total tries
        initial_wait: Time to first retry
        backoff_factor: Backoff multiplier (e.g. value of 2 will double the delay each retry).
        logger: logger to be used, if none specified print
    """

    def retry_decorator(f):
        @wraps(f)
        def func_with_retries(*args, **kwargs):
            _tries, _delay = total_tries + 1, initial_wait
            while _tries > 1:
                try:
                    # log(f"{total_tries + 2 - _tries}. try:", logger)
                    return f(*args, **kwargs)
                except exceptions as e:
                    _tries -= 1
                    # print_args = args if args else "no args"
                    if _tries == 1:
                        msg = str(
                            f"Function: {f.__name__}\n"
                            f"Failed despite best efforts after {total_tries} tries."
                        )
                        log(msg, logger)
                        raise
                    msg = str(
                        # f"Function: {f.__name__}\n"
                        # f"Exception: {e}\n"
                        f"Retrying in {_delay} seconds!\n"
                    )
                    log(msg, logger)
                    time.sleep(_delay)
                    _delay *= backoff_factor

        return func_with_retries

    return retry_decorator
