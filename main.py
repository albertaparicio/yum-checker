#!/usr/bin/env python

"""
Yum Checker
Copyright (C) 2020  Albert Aparicio Isarn

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

See COPYING for the full license
"""

import argparse
import json
import os
from pathlib import Path
from typing import List

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import Firefox, Chrome
from selenium.webdriver.chrome.options import Options as ChromeOpts
from selenium.webdriver.firefox.options import Options as FirefoxOpts
from selenium.webdriver.remote.webdriver import WebDriver
from tqdm import tqdm

from utils import retry


def read_file_codes(code_list_path: Path) -> List[str]:
    # Read codes from the file
    assert code_list_path.exists(), "Code list path does not exist"

    with code_list_path.open("r") as f:
        code_lines = f.readlines()

    yum_codes = [code.strip() for code in code_lines]

    return yum_codes


def read_ocr_json_codes(ocr_json_path: Path) -> List[str]:
    assert ocr_json_path.exists(), "OCR data path does not exist"

    with ocr_json_path.open("r") as j:
        ocr_data = json.load(j)

    yum_codes = [
        line.strip()
        for line in ocr_data["textAnnotations"][0]["description"].split()
        if "-" in line
    ]

    return yum_codes


def _set_browser_options(options):
    options.headless = True
    assert options.headless  # Operating in headless mode


def init_browser(args) -> WebDriver:
    if args.browser == "firefox":
        options = FirefoxOpts()
        _set_browser_options(options)
        browser = Firefox(options=options, service_log_path=os.path.devnull)

    else:
        options = ChromeOpts()
        _set_browser_options(options)
        browser = Chrome(options=options, service_log_path=os.path.devnull)

    return browser


@retry(NoSuchElementException, total_tries=10)
def _check_code(yum_code: str, browser: WebDriver) -> bool:
    browser.get(f"https://bandcamp.com/yum?code={yum_code}")

    # Look at the redeem button
    try:
        redeem_button = browser.find_element_by_class_name("redeem-button")
    except NoSuchElementException:
        if "Too Many Requests" in browser.title:
            raise
        else:
            # We have moved directly into the Download page, so the code is valid
            return True

    # Look at the green icon in the textbox
    valid_icon = browser.find_element_by_id("code-icon")
    icon_height = valid_icon.size["height"]
    icon_width = valid_icon.size["width"]

    return (icon_height > 0 and icon_width > 0) and not bool(
        redeem_button.get_attribute("disabled")
    )


def check_code_list(browser, yum_codes):
    valid_codes = []

    for code in tqdm(yum_codes):
        code_valid = _check_code(code, browser)

        if code_valid:
            if len(valid_codes) == 0:
                tqdm.write("Valid codes:\n------------")

            valid_codes.append(code)
            tqdm.write(f"{code}")
    tqdm.write(f"Total: {len(valid_codes)} valid codes")


def main(args):
    yum_codes = args.codes
    code_list_path = args.list
    ocr_json_path = args.ocr

    assert (yum_codes is not None) ^ (code_list_path is not None) ^ (ocr_json_path is not None), (
        "Please pass either one or more codes "
        "or the path to a code list "
        "or the path to an OCR JSON output"
    )

    if code_list_path is not None:
        yum_codes = read_file_codes(Path(code_list_path))

    elif ocr_json_path is not None:
        yum_codes = read_ocr_json_codes(Path(ocr_json_path))

    if args.verbose:
        tqdm.write("Detected Codes:")
        tqdm.write("\n".join(yum_codes))
        tqdm.write("\n")

    browser = init_browser(args)

    check_code_list(browser, yum_codes)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Check validity of Bandcamp yum codes")
    parser.add_argument(
        "-c", "--codes", type=str, nargs="+", help="One or several codes to be checked"
    )
    parser.add_argument(
        "-l", "--list", type=str, help="Path to a text file with a list of codes to be checked"
    )
    parser.add_argument(
        "-o",
        "--ocr",
        type=str,
        help="Path to a JSON file with the output of Google's OCR on a codes image",
    )
    parser.add_argument(
        "-b", "--browser", type=str, choices=["firefox", "chrome"], default="firefox"
    )
    parser.add_argument("-v", "--verbose", action="store_true")

    opts = parser.parse_args()

    main(opts)

    exit()
