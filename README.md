# Yum Checker

This tool allows to check the validity of one or more yum codes from Bandcamp

## Requirements

In order to use this tool you will need to have installed a webdriver supported by Selenium, such as `geckodriver` for Firefox or `ChromeDriver` for Chrome.

This tool has been implemented with Python 3.6 and later versions in mind. Versions prior to 3.6 may work, but are not officially supported.

Required packages can be installed with `$ pip install -r requirements.txt`

## Usage

The usage of this tool can be retrieved with `$ python main -h`. Below are some examples:

### Check single code

`$ python main.py -c "yum_code"`

### Check several codes

`$ python main.py -c "yum_code_1" "yum_code_2" "yum_code_3" ...  "yum_code_N"`

### Check codes from a file

`$ python main.py -l code_list.txt`

In this example, `code_list.txt` contains multiple codes, one on each line.

### Check codes from a yum code screenshot

Sometimes artists and labels distribute an image with a bunch of codes. For this tool to be able to check them, we need to use an Optical Character Recognition system (OCR).

The `-o / --ocr` option takes the JSON output of the OCR from Google Cloud Vision and checks the detected codes.

If you want to try out this feature, the Cloud Vision API has a [free online demo](https://cloud.google.com/vision/docs/drag-and-drop).

Once you have the output of this API in a JSON file, pass it to the tool like this:

`$ python main.py -o ocr_output.json`

### Adjusting which webdriver to use

`$ python main.py -b firefox`

The default webdriver is Firefox's, but you can use ChromeDriver by passing `chrome` instead of `firefox`.

## Contributing

This tool is simple and meant to fulfill a single purpose. Still, if you find a missing feature or a bug, feel free to send a pull request and/or an issue.

## License

Yum Checker
Copyright (C) 2020  Albert Aparicio Isarn

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program (see [COPYING](COPYING)).  If not, see <http://www.gnu.org/licenses/>.